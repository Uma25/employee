package ru.patyukov.employeegadget.employeegadget.controller;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.patyukov.employeegadget.employeegadget.dto.EmployeeDto;
import ru.patyukov.employeegadget.employeegadget.service.Impl.EmployeeServiceImpl;

import java.util.List;

@RequestMapping("/employee")
@RestController
public class EmployeeController {
   private final EmployeeServiceImpl employeeServiceImpl;

   public EmployeeController(EmployeeServiceImpl employeeServiceImpl) {
        this.employeeServiceImpl = employeeServiceImpl;
    }

    @GetMapping("/id")
    public EmployeeDto findById(@RequestParam Long id){
        return employeeServiceImpl.findEmployeeById(id );
    }

    @GetMapping("/all")
    public List<EmployeeDto> findAll(){
        return employeeServiceImpl.findAllEmployee();
    }

    @PostMapping("/save")
    public EmployeeDto save(@RequestBody EmployeeDto employeeDto){
    return employeeServiceImpl.saveEmployee(employeeDto);
}

    @PutMapping("/update")
    public EmployeeDto update(@RequestBody EmployeeDto employeeDto){
     return employeeServiceImpl.updateEmployee(employeeDto);
    }

    @DeleteMapping("/delete")
    public String delete(@RequestParam Long id) {
        return employeeServiceImpl.deleteEmployee(id);

    }
}