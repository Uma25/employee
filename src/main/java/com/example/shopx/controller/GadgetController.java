package ru.patyukov.employeegadget.employeegadget.controller;
import org.springframework.web.bind.annotation.*;
import ru.patyukov.employeegadget.employeegadget.dto.GadgetDto;
import ru.patyukov.employeegadget.employeegadget.service.Impl.GadgetServiceImpl;

import java.util.List;

@RequestMapping("/gadget")
@RestController

public class GadgetController {

    private final GadgetServiceImpl gadgetService;

    public GadgetController(GadgetServiceImpl gadgetService) {
        this.gadgetService = gadgetService;
    }

    @GetMapping("/id")
    public GadgetDto get(@RequestParam Long id) {
        return gadgetService.findById(id);
    }

    @GetMapping("/all")
    public List<GadgetDto> getAll(){
        return gadgetService.findAll();
    }

    @PostMapping("/save")
    public GadgetDto save(@RequestBody GadgetDto gadgetDto) {
        return gadgetService.saveGadget(gadgetDto);
    }

    @PutMapping("/update")
    public GadgetDto update(@RequestBody GadgetDto gadgetDto){
        return gadgetService.update(gadgetDto);
    }

    @DeleteMapping("/delete")
    public String delete(@RequestParam Long id){
         gadgetService.delete(id);
        return "Deleted";
    }
}

