package ru.patyukov.employeegadget.employeegadget.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.patyukov.employeegadget.employeegadget.entity.Employee;

@Repository
public interface EmployeeRepository extends CrudRepository<Employee, Long> {



}
