package ru.patyukov.employeegadget.employeegadget.repository;


import org.springframework.data.repository.CrudRepository;
import ru.patyukov.employeegadget.employeegadget.entity.Gadget;

public interface GadgetRepository extends CrudRepository<Gadget, Long> {

}
