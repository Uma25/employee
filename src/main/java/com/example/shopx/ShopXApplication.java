package com.example.shopx;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ShopXApplication {
    public static void main(String[] args) {
        SpringApplication.run(ShopXApplication.class, args);
    }

}
